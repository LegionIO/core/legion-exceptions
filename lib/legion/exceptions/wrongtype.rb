# frozen_string_literal: true

require_relative 'wrongtypes/array'
require_relative 'wrongtypes/hash'
require_relative 'wrongtypes/integer'
require_relative 'wrongtypes/nil'
require_relative 'wrongtypes/string'

module Legion
  module Exception
    module WrongType
    end
  end
end
