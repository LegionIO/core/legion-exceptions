# frozen_string_literal: true

module Legion
  module Exception
    class HandledTask < StandardError
    end
  end
end
