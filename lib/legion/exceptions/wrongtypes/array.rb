# frozen_string_literal: true

module Legion
  module Exception
    module WrongType
      class Array < TypeError
      end
    end
  end
end
