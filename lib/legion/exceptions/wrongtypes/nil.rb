# frozen_string_literal: true

module Legion
  module Exception
    module WrongType
      class Nil < TypeError
      end
    end
  end
end
