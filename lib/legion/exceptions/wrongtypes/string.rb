# frozen_string_literal: true

module Legion
  module Exception
    module WrongType
      class String < TypeError
      end
    end
  end
end
