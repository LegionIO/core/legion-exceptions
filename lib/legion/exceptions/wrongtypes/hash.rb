# frozen_string_literal: true

module Legion
  module Exception
    module WrongType
      class Hash < TypeError
      end
    end
  end
end
