# frozen_string_literal: true

begin
  require 'simplecov'
  SimpleCov.start do
    @filters = []
    add_filter '/spec/'
    add_filter '/tmp/'
    add_filter 'Gemfile'
    add_filter '*.gemspec'
    add_filter 'lib/legion/exceptions/version.rb'

    add_group 'WrongType', 'exceptions/wrongtypes'
    add_group 'Exceptions', 'exceptions'
    track_files 'lib/**/*.rb'
    track_files 'lib/**/**/*.rb'
    project_name 'Legion::Exceptions'
    use_merging true
    formatter SimpleCov::Formatter::SimpleFormatter if ENV.key? 'SONAR_TOKEN'
  end

  SimpleCov.minimum_coverage 95 unless ENV.key? 'SONAR_TOKEN'
rescue LoadError
  puts 'Failed to load file for coverage reports, continuing without it'
end

RSpec.configure do |config|
  config.example_status_persistence_file_path = '.rspec_status'
  config.disable_monkey_patching!
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

require 'bundler/setup'
require 'legion/exceptions'
