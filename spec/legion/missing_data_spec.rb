# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/missing_data'

RSpec.describe Legion::Exception::MissingData do
  it { is_expected.to be_a_kind_of RuntimeError }
end
